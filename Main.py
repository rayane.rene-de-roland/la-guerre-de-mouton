import random
from random import randint
import matplotlib.pyplot as plt

class Monde:
  def __init__(self,dimension,duree_repousse):
    """
             dimension : Entier, représente la taille du côté
        duree_repousse : Entier, vitesse de repousse de l'herbe (intervale de pousse lors de la simulation)
                 carte : Liste de listes, Matrice (dimension x dimension)
        compteur_case_herbe : nombre de case contenant de l'herbe
        """
    self.dimension,self.duree,self.carte,self.compteur_case_herbe = dimension, duree_repousse,[[0 for i in range(dimension)] for j in range(dimension)],0
  def __repr__(self):
    """Affiche la matrice carte"""
    return str(self.carte)
  def herbePousse(self):
    """Fait repousser l'herbe dans les cases de la matrice monde"""
    for i in range(self.dimension):
      for j in range(self.dimension):
        if self.carte[i][j] == 0 :
          self.carte[i][j]+=1
  def herbeMange(self,i,j):
    """i et j sont les indices du coefficient de la matrice carte"""
    self.carte[i][j]=self.carte[i][j]-1
    return self.carte
  def nbHerbe(self):
    """Retourne le nombre de cases qui contiennent de l'herbe"""
    self.compteur_case_herbe=0
    for i in range(self.dimension):
      for j in range(self.dimension):
        if self.carte[i][j]>0:
          self.compteur_case_herbe+=1
    return self.compteur_case_herbe
  def getCoefCarte(self,i,j):
    """Accesseur de type particulier qui renvoie la valeur du coefficient a(i,j) de la carte. i et j sont les indices du coefficient de la matrice carte"""
    return self.carte[i][j]

    
class Mouton():
  def __init__(self,Kcal,eng,repro,monde):
    """
                         monde : Objet, monde dans lequel se trouve le mouton
               Kcal : Nombre, gain d'énergie apporté par la consommation d'un carré d'herbe
        x, y : Couple x,y qui représente la position du mouton
                       eng : Entier positif (ou nul), quand l'énergie d'un mouton est à 0, il meurt et l'objet est supprimé. Initialisé à 1*Kcal ou 2*Kcal
             reprod : Entiers compris entre 0 et 99, ce taux de représente le pourcentage de chance qu'un mouton se reproduise
             sexe : le sexe de moutons générés qui est aléatoirement une Femmelle F ou un Mâle M
        """
    sexe = ['M','F']
    self.Kcal, self.eng, self.reprod, self.monde = Kcal,eng*Kcal,repro-1,monde
    self.x, self.y =randint(0,self.monde.dimension),randint(0,self.monde.dimension)
    self.sexe= random.choice(sexe)
    self.energie = eng
  def variation_energie(self):
    """Diminue de 1 l'énergie du mouton s'il n'est pas sur un carré d'herbe, sinon augmente de Kcal. Renvoie energie"""
    if self.monde.carte[self.x][self.y] > 0:
      self.monde.herbeMange(self.x,self.y)
      self.eng += self.Kcal
      return self.eng
    else:
      self.eng -= 1
      return self.eng
      
  def place_mouton(self,i,j):
    """Place le mouton sur les coordonnées de la case adjactente séléctionner sur le fonction déplacement"""
    self.x,self.y = i,j
    
  def deplacement(self):
    """Déplace le mouton sur l'une des 8 cases adjacentes à la sienne"""
    chif = [-1,0,1]
    i = self.x + random.choice(chif)
    j = self.y + random.choice(chif)
    if i > (self.monde.dimension-1):
      i = i - self.monde.dimension
    if j > (self.monde.dimension-1):
      j = j - self.monde.dimension
    if i < 0 :
      i = i + self.monde.dimension
    if j < 0:
      j = j + self.monde.dimension
    self.place_mouton(i,j)
    self.variation_energie()

class Simulation:
  def __init__(self,fin,mouton,nbm,monde,loup,nbl,nb_ch):
    """
           nombre_moutons : Entier, nombre de moutons initialement présents sur la carte
                  horloge : Entier, initialisé à 0
             fin_du_monde : Entier, temps maximum de la simulation
                  moutons : Liste d'objets Moutons
                chasseurs : Liste contenant les objets chasseurs
                  loups   : Liste d'objets Loups
                    monde : Instance de la classe Monde
          resultats_herbe : variable donnant le nombre d'herbe à un instant t
        resultats_moutons : varaiable qui est un entier indiquant le nombre de moutons contenu dans moutons
                nb_loups  : variable qui est un entier indiquant le nombre de Loups
                lim_m     : Limite de reproduction des moutons
                lim_l     : Limite de reproduction de loups
                nb_ch : nombre de chasseurs dans la simulation
        """
    self.nombre_moutons,self.horloge,self.fin_du_monde,self.moutons,self.monde,self.loups,self.mod_m,self.mod_l = nbm,0,fin,[],monde,[],mouton,loup
    self.chasseurs= []
    for i in range(nbm):
      self.moutons.append(Mouton(mouton.Kcal,mouton.eng,mouton.reprod,monde))
    for b in range(nbl):
      self.loups.append(Loup(monde,loup.reprod))
    for x in range(nb_ch):
      self.chasseurs.append(Chasseurs(monde))
    self.lim_m = self.monde.dimension**2
    self.lim_l = ((self.monde.dimension**2)//2)
    self.resultats_herbe = self.monde.nbHerbe()
    self.nb_loups = len(self.loups)
    self.resultats_moutons = len(self.moutons)

  
  def Sim_lv1(self):
    """Gère la simulation en créant une boucle. Renvoie un graphe avec deux courbes indiquant le nombre de Moutons et d'herbe à la fin de son éxécution"""
    resultats = []
    cpt = 0
    self.monde.herbePousse()
    while 0<self.nombre_moutons<self.lim_m  and self.horloge != self.fin_du_monde:
      self.horloge+=1
      if cpt == self.monde.duree:
        self.monde.herbePousse()
        cpt = 0
      if cpt < self.monde.duree:
        cpt+=1
      for i in range(len(self.moutons)):
        if self.moutons[i].eng == 0:
          del self.moutons[i]
        else:
          self.moutons[i].deplacement()
      for w in range(len(self.moutons)):
        spawn  = random.randint(0,100)
        if spawn <= self.moutons[0].reprod:
          self.moutons.append(Mouton(self.mod_m.Kcal,self.mod_m.eng,self.mod_m.reprod,self.mod_m.monde))
      for i in range(len(self.moutons)):
        self.moutons[i].deplacement()
      self.resultats_herbe = self.monde.nbHerbe()
      self.resultats_moutons = len(self.moutons)
      self.nombre_moutons = len(self.moutons)
      resultats.append((self.resultats_herbe,self.nombre_moutons))
    sheep= [i[1] for i in resultats]
    grass = [i[0] for i in resultats]
    time = [i for i in range(len(resultats))]
    plt.plot(time,sheep,label = "Mouton")
    plt.plot(time,grass,label = "Herbe")
    plt.legend()
            
                                


  def Sim_lv2(self):
    """Gère la simulation en créant une boucle. Renvoie un graphe avec trois courbes indiquant le nombre de Moutons, d'herbe et de Loups à la fin de son éxécution, les Loups sont capables de chassers les moutons se trouvant sur les 8 cases leurs étant adjacentes"""
    resultats = []
    chif = [-1,0,1]
    k=None
    cpt = 0
    self.monde.herbePousse()
    while self.resultats_moutons<self.lim_m  and self.horloge != self.fin_du_monde and self.resultats_moutons!=0:
      self.horloge+=1
      for w in range(len(self.moutons)):
        spawn  = random.randint(0,100)
        if spawn <= self.mod_m.reprod:
          self.moutons.append(Mouton(self.mod_m.Kcal,self.mod_m.eng,self.mod_m.reprod,self.mod_m.monde))
      if len(self.loups)< self.lim_l:
        for i in range(len(self.loups)):
          spawn  = random.randint(0,100)
          if spawn <= self.mod_l.reprod:
            self.loups.append(Loup(self.mod_l.monde,self.mod_l.reprod))
      if cpt == self.monde.duree:
        self.monde.herbePousse()
        cpt = 0
      if cpt < self.monde.duree:
        cpt+=1
      for i in range(len(self.moutons)):
        if self.moutons[i].eng == 0:
          del self.moutons[i]
      for i in range(len(self.moutons)):
          self.moutons[i].deplacement()    
      for f in self.loups:
        if f.eng == 0:
          self.loups.remove(f)   
      for i in range(len(self.loups)):
          for v in range(len(self.moutons)):
            for w in chif:
              for z in chif:
                if (self.loups[i].x + w) == self.moutons[v].x and (self.loups[i].y +z) == self.moutons[v].y:
                  self.loups[i].place_loups(self.moutons[v].x,self.moutons[v].y)
          k = self.loups[i].variation_energie(self.moutons)
          if k != None:
            del self.moutons[k]
            k = None
          self.loups[i].Deplacement()              
      self.resultats_herbe = self.monde.nbHerbe()
      self.nb_loups = len(self.loups)
      self.resultats_moutons = len(self.moutons)
      resultats.append((self.resultats_herbe,self.nb_loups,self.resultats_moutons))
    sheep= [i[2] for i in resultats]
    wolf = [i[1] for i in resultats]
    grass = [i[0] for i in resultats]
    time = [i for i in range(len(resultats))]
    plt.plot(time,sheep,label = "Moutons")
    plt.plot(time,grass,label = "Herbes")
    plt.plot(time,wolf,label = "Loups")
    plt.legend()

  def Sim_lv3(self):
    """Gère la simulation en créant une boucle. Renvoie un graphe avec trois courbes indiquant le nombre de Moutons, d'herbe et de Loups à la fin de son éxécution, les Loups sont capables de chassers les moutons se trouvant sur les 8 cases leurs étant adjacentes et dans le même cas des chasseurs apparaissent et sont capables de faire de même avec les Loups"""
    resultats = []
    chif = [-1,0,1]
    k=None
    hunt = None
    cpt = 0
    self.monde.herbePousse()
    while self.resultats_moutons<self.lim_m  and self.horloge != self.fin_du_monde and self.resultats_moutons!=0:
      self.horloge+=1
      for w in range(len(self.moutons)):
        spawn  = random.randint(0,100)
        if spawn <= self.mod_m.reprod:
          self.moutons.append(Mouton(self.mod_m.Kcal,self.mod_m.eng,self.mod_m.reprod,self.mod_m.monde))
      if len(self.loups)< self.lim_l:
        for i in range(len(self.loups)):
          spawn  = random.randint(0,100)
          if spawn <= self.mod_l.reprod:
            self.loups.append(Loup(self.mod_l.monde,self.mod_l.reprod))
      if cpt == self.monde.duree:
        self.monde.herbePousse()
        cpt = 0
      if cpt < self.monde.duree:
        cpt+=1
      for i in self.moutons:
        if i.eng == 0:
          self.moutons.remove(i)
      for i in range(len(self.moutons)):
          self.moutons[i].deplacement()    
      for f in self.loups:
        if f.eng == 0:
          self.loups.remove(f)   
      for i in range(len(self.loups)):
          for v in range(len(self.moutons)):
            for w in chif:
              for z in chif:
                if (self.loups[i].x + w) == self.moutons[v].x and (self.loups[i].y +z) == self.moutons[v].y:
                  self.loups[i].place_loups(self.moutons[v].x,self.moutons[v].y)
          k = self.loups[i].variation_energie(self.moutons)
          if k != None:
            del self.moutons[k]
            k = None
          self.loups[i].Deplacement()
      for i in range(len(self.chasseurs)):
          for d in range(len(self.loups)):
            for w in chif:
              for z in chif:
                if (self.chasseurs[i].x + w) == self.loups[d].x and (self.chasseurs[i].y +z) == self.loups[d].y:
                  self.chasseurs[i].place_chasseurs(self.loups[d].x,self.loups[d].y)
          hunt = self.chasseurs[i].Chasse(self.loups)
          if hunt != None:
            del self.loups[hunt]
            hunt= None
          self.chasseurs[i].deplacement()
      self.resultats_herbe = self.monde.nbHerbe()
      self.nb_loups = len(self.loups)
      self.resultats_moutons = len(self.moutons)
      resultats.append((self.resultats_herbe,self.nb_loups,self.resultats_moutons))
    sheep= [i[2] for i in resultats]
    wolf = [i[1] for i in resultats]
    grass = [i[0] for i in resultats]
    time = [i for i in range(len(resultats))]
    plt.plot(time,sheep,label = "Moutons")
    plt.plot(time,grass,label = "Herbes")
    plt.plot(time,wolf,label = "Loups")
    plt.legend()
    
  def Sim_lv4(self):
    """Gère la simulation en créant une boucle. Renvoie un graphe avec trois courbes indiquant le nombre de Moutons, d'herbe et de Loups à la fin de son éxécution, les Loups sont capables de chassers les moutons se trouvant sur les 8 cases leurs étant adjacentes et dans le même cas des chasseurs apparaissent et sont capables de faire de même avec les Loups. Les Moutons et les Loups peuvent se reproduire naturellement une fois que deux individus de la même espèce et de différents sexes se trouvent sur la même case ajoutant alors un mouton au monde"""
    resultats = []
    chif = [-1,0,1]
    v=None
    hunt = None
    cpt = 0
    self.monde.herbePousse()
    while self.resultats_moutons<self.lim_m  and self.horloge != self.fin_du_monde and self.resultats_moutons!=0:
      self.horloge+=1
      for w in range(len(self.moutons)):
        for n in range(len(self.moutons)):
          if self.moutons[w].x == self.moutons[n].x and self.moutons[w].y == self.moutons[n].y and self.moutons[w].sexe != self.moutons[n].sexe:
            self.moutons.append(Mouton(self.mod_m.Kcal,self.mod_m.eng,self.mod_m.reprod,self.mod_m.monde))
      if len(self.loups)< self.lim_l:
        for w in range(len(self.loups)):
          for n in range(len(self.loups)):
            if w!=n and self.loups[w].x == self.loups[n].x and self.loups[w].y == self.loups[n].y and self.loups[w].sexe != self.loups[n].sexe:
              self.loups.append(Loup(self.mod_l.monde,self.mod_l.reprod))
      if cpt == self.monde.duree:
        self.monde.herbePousse()
        cpt = 0
      if cpt < self.monde.duree:
        cpt+=1
      for i in self.moutons:
        if i.eng == 0:
          self.moutons.remove(i)
      for i in range(len(self.moutons)):
          self.moutons[i].deplacement()    
      for f in self.loups:
        if f.eng == 0:
          self.loups.remove(f)   
      for i in range(len(self.loups)):
          for d in range(len(self.moutons)):
            for w in chif:
              for z in chif:
                if (self.loups[i].x + w) == self.moutons[d].x and (self.loups[i].y +z) == self.moutons[d].y:
                  self.loups[i].place_loups(self.moutons[d].x,self.moutons[d].y)
          v = self.loups[i].variation_energie(self.moutons)
          if v != None:
            del self.moutons[v]
            v = None  
          self.loups[i].Deplacement()
      for i in range(len(self.chasseurs)):
          for d in range(len(self.loups)):
            for w in chif:
              for z in chif:
                if (self.chasseurs[i].x + w) == self.loups[d].x and (self.chasseurs[i].y +z) == self.loups[d].y:
                  self.chasseurs[i].place_chasseurs(self.loups[d].x,self.loups[d].y)
          hunt = self.chasseurs[i].Chasse(self.loups)
          if hunt != None:
            del self.loups[hunt]
            hunt= None
          self.chasseurs[i].deplacement()
      self.resultats_herbe = self.monde.nbHerbe()
      self.nb_loups = len(self.loups)
      self.resultats_moutons = len(self.moutons)
      resultats.append((self.resultats_herbe,self.nb_loups,self.resultats_moutons))
    sheep= [i[2] for i in resultats]
    wolf = [i[1] for i in resultats]
    grass = [i[0] for i in resultats]
    time = [i for i in range(len(resultats))]
    plt.plot(time,sheep,label = "Moutons")
    plt.plot(time,grass,label = "Herbes")
    plt.plot(time,wolf,label = "Loups")
    plt.legend()
  

class Loup():
  def __init__(self,monde,taux_de_reproduction):
    """
    monde : Objet, monde dans lequel se trouve le mouton
    taux_reproduction : Entiers compris entre 1 et 100, ce taux de représente le pourcentage de chance qu'un mouton se reproduise
    sexe : le sexe du Loup généré qui est aléatoirement une Femmelle F ou un Mâle M
    eng : énergie initiale du Loup initialisé à 10 et si elle atteint 0 le loup est éliminé de la simulation
    Kcal : énergie que le Loup gagne en mangeant un mouton
    """
    sexe = ['M','F']
    self.Kcal, self.eng, self.reprod, self.monde = 18,10,taux_de_reproduction-1,monde
    self.x, self.y =randint(0,self.monde.dimension),randint(0,self.monde.dimension)
    self.sexe= random.choice(sexe)
    
  def variation_energie(self,moutons):
    """Diminue de 1 l'énergie du Loup s'il n'y pas de moutons sur sa case, sinon augmente de Kcal. Renvoie l'emplacement du mouton consommé si un se trouve sur sa case et dans le cas contraire Renvoie None"""
    for i in range(len(moutons)):
      if self.x == moutons[i].x and self.y == moutons[i].y:
        self.eng += self.Kcal
        return i
    self.eng -= 1
    return None
      
  def place_loups(self,i,j):
    """Place le Loup sur les coordonnées i et j dans Deplacement"""
    self.x,self.y = i,j
    
  def Deplacement(self):
    """Choisit une case aléatoire parmis les 8 cases adjacentes afin d'y placer le Loup"""
    chif = [-1,0,1]
    i = self.x + random.choice(chif)
    j = self.y + random.choice(chif)
    if i > (self.monde.dimension-1):
      i = i - self.monde.dimension
    if j > (self.monde.dimension-1):
      j = j - self.monde.dimension
    if i < 0 :
      i = i + self.monde.dimension
    if j < 0:
      j = j + self.monde.dimension
    self.place_loups(i,j)

class Chasseurs():
  def __init__(self,world):
    """monde : Objet, monde dans lequel se trouve le chasseur
      x et y : Les coordonnées du chasseur
    """
    self.monde = world
    self.x,self.y = randint(0,self.monde.dimension),randint(0,self.monde.dimension)
  def Chasse(self,loups):
    """Vérifie dans la liste de Loup si un se trouve sur les même coordonnées que le chasseur et si c'est le cas renvoie son emplacement dans la liste et dans le cas contraire renvoie None"""
    for i in range(len(loups)):
      if self.x == loups[i].x and self.y == loups[i].y:
        return i
    return None
  def place_chasseurs(self,i,j):
    """Place le chasseur sur des coordonnées i et j"""
    self.x,self.y = i,j
  def deplacement(self):
    """Choisit une case aléatoire parmis les 8 cases adjacentes afin d'y placer le chasseur"""
    chif = [-1,0,1]
    i = self.x + random.choice(chif)
    j = self.y + random.choice(chif)
    if i > (self.monde.dimension-1):
      i = i - self.monde.dimension
    if j > (self.monde.dimension-1):
      j = j - self.monde.dimension
    if i < 0 :
      i = i + self.monde.dimension
    if j < 0:
      j = j + self.monde.dimension
    self.place_chasseurs(i,j)
    
    