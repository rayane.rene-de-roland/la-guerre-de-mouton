# La Guerre des Moutons
**Rayane RENE DE ROLAND TG6 Spé NSI**

Ce projet consistera à modéliser le fonctionnement d'un écosystème:
- De l'herbe et des moutons
- Des loups
- Des Chasseurs

Dans la simulation, les `moutons` et les `loups` se déplacent, les `loups` mangent les `moutons` ou périssent, tout comme les `moutons` qui doivent manger de `l'herbe` afin de survivre. Les `chasseurs` chassent les `loups` afin de diminuer leur nombre. `L'herbe` doit pouvoir pousser.

A travers mon projet, j'ai créé 5 classes distincts qui son Chasseurs, Mouton, Monde, Loup et Simulation pour réaliser ce projet

## Pour lancer la simulation de simulation plusieurs étapes sont nécessaires :

* La création d'un Monde avec une commande de ce type ``` monde = Monde(x,v)``` avec comme première valeur c'est à dire x comme taille de la matrice carré qui forme le monde et la valeur 4 qui correspond au temps nécessaire pour que l'herbe pousse(cette valeur doit obligatoirement être supérieure ou égale à 1)

* La seconde étape serait la création d'un mouton avec la commande de type ``` mouton = Mouton(Kcal,eng,reprod,monde) ```  avec comme premier paramètre le gain  `Kcal` après la consommation d'herbe qui serait conseillé à 4, le second paramètre est l'énergie initiale `eng` du mouton qui sera comprise entre 1 et 2 puis serait multiplier par `Kcal`, le troisième paramètre est la chance de reproduction du mouton qui dans une situation basique aurait reprod/100 de chance de se cloner afin de donner naissance à un nouveau mouton, le quatrième paramètre ` monde ` désigne le monde où le mouton va se trouver.

* La troisième étape serait la création d'un loup avec la commande de type ``` loup = Loup(monde,reprod) ```, le premier paramètre ` monde ` désigne le monde où le loup va se trouver et le second paramètre est la chance de reproduction du loup qui dans une situation basique aurait reprod/100 de chance de se cloner afin de donner naissance à un nouveau loup.

* La quatrième étape serait la création de la simulation où vont intéragir loups,moutons et chasseurs avec la commande ``` simulation = Simulation(fin,mouton,nbm,monde,loup,nbl,nb_ch)```, tous d'abord les paramètres `nbm`, `nbl` et `nb_ch` désignent respéctivement le nombre initiale de moutons, loups et chasseurs(le nombre de chasseur ne varie pas au cours de la simulation) au début de la simulation. Le paramètre `fin` désigne la durée de la simulation. Les paramètres `loup`,`mouton` et `monde` correspondent aux éléments crées durant les trois étapes précedentes.

## Les différents paramètres de la simulation ont été introduites, il ne reste plus qu'a la lancé. Pour cela vous avez le choix entre 4 types de simulations:


* `Sim_lv1()` qui est une simulation basique avec uniquement des moutons(qui mangeront de l'herbe) dans l'environnement pour l'éxécuter à partir de l'exemple précédent, la commande ci contre est à taper pour l'effectuer ```simulation.Sim_lv1()```. Un graphique apparaitra à l'issu de cette ligne de code qui comporte 2 courbe, l'une qui représente la quantité d'herbe et l'autre le nombre de moutons

* `Sim_lv2()` qui est une simulation comportant des moutons(qui mangeront de l'herbe) et des loups(qui tueurons des moutons) intéragissant dans l'environnement. Pour l'éxécuter à partir de l'exemple précédent, la commande ci contre est à taper pour l'effectuer ```simulation.Sim_lv2()```. Un graphique apparaitra à l'issu de cette ligne de code qui comporte 3 courbe, l'une qui représente la quantité d'herbe et l'autre le nombre de moutons et la dernière indiquant le nombre de moutons

* `Sim_lv3()` qui est une simulation comportant des moutons(qui mangeront de l'herbe), des loups(qui tueuront des moutons) et des chasseurs(qui tueuront des loups) intéragissant dans l'environnement. Pour l'éxécuter à partir de l'exemple précédent, la commande ci contre est à taper pour l'effectuer ```simulation.Sim_lv3()```. Un graphique apparaitra à l'issu de cette ligne de code qui comporte 3 courbe, l'une qui représente la quantité d'herbe et l'autre le nombre de moutons et la dernière indiquant le nombre de moutons

* `Sim_lv4()` qui est une simulation comportant des moutons(qui mangeront de l'herbe), des loups(qui tueuront des moutons) et des chasseurs(qui tueuront des loups) intéragissant dans l'environnement. Pour l'éxécuter à partir de l'exemple précédent, la commande ci contre est à taper pour l'effectuer ```simulation.Sim_lv4()```. Au cours de cette simulation les loups et moutons se reproduisent de façon naturel pour plus d'information lire le code. Un graphique apparaitra à l'issu de cette ligne de code qui comporte 3 courbe, l'une qui représente la quantité d'herbe et l'autre le nombre de moutons et la dernière indiquant le nombre de moutons

* Les Loups savent chasser des moutons dans les cases adjacentes et les chasseurs savent chasser des loups dans les cases adjacentes et ils ne peuvent chacun chasser qu'une fois par tour.

## Exemples de lignes de Codes pour lancer la Simulation:

***Code en Langage python***

```
    monde = Monde(50,2)
    mouton = Mouton(4,2,10,A)
    loup = Loup(A,10)
    simulation = Simulation(100,mouton,2000,monde,loup,900,0)
    simulation.Sim_lv4()
```

```
    monde = Monde(25,1)
    mouton = Mouton(4,2,10,A)
    loup = Loup(A,10)
    simulation = Simulation(100,mouton,20,monde,loup,0,0)
    simulation.Sim_lv1()
```

```
    monde = Monde(40,5)
    mouton = Mouton(4,2,10,A)
    loup = Loup(A,10)
    simulation = Simulation(100,mouton,300,monde,loup,40,0)
    simulation.Sim_lv2()
```